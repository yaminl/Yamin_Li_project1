#include "A_star.h"

namespace AStarAlgorithms
{
	void A_star::clear()
	{
		CLOSE_.clear();
		OPENMap_.clear();
		while (!OPEN_.empty())
			OPEN_.pop();
		while (!CurrentAStarPathStack_.empty())
			CurrentAStarPathStack_.pop();
		CurrentAStarPathVector2Show_.clear();
	}

	int A_star::findPath()
	{
		// From start point, which is current car position, expanding will index all the expanding points
		std::shared_ptr<Node> Expanding = std::make_shared<Node>(CurrentCarPos_, getHeuristicForPos(CurrentCarPos_));

		// push start point into OPEN list to start the algorithm
		OPEN_.push(Expanding);
		OPENMap_[Expanding->pos_.cvt2Key()] = (Expanding); // hashmap key: postion, value: shared_ptr of nodes

		// when End point is not in CLOSE
		while (CLOSE_.find(GoalPos_.cvt2Key()) == CLOSE_.end())
		{
			if (OPENMap_.empty())
			{
				std::cout << "##### Can not find path to the destination!!! #####" << std::endl;
				break;
				return 0;
			}

			Expanding = OPEN_.top();
			OPEN_.pop();
			auto iter = OPENMap_.find(Expanding->pos_.cvt2Key());  // at the same time open set delete the expanding too
			OPENMap_.erase(iter);

			// insert the Expanding node into CLSOE_
 			CLOSE_[Expanding->pos_.cvt2Key()] = Expanding;

			// for all the neigbors
			std::shared_ptr<Node> neighbor;

			std::vector<position> MoveDirection = { position(-1, 0), position(0,-1),
				position(1, 0), position(0, 1) }; // up, left, down, right, any order  

			for (auto pos : MoveDirection)
			{
				position NewPos = pos + Expanding->pos_;
					
				if ((NewPos.x_ > MapRows_ - 1) || (NewPos.x_ < 0) || // rows out of range
					(NewPos.y_ > MapCols_ - 1) || (NewPos.y_ < 0) || // cols out of range
					(map_.find(Expanding->pos_.cvt2Key()) != map_.end() &&
						map_.at(Expanding->pos_.cvt2Key())->CanStepOn_ == MapPoint::MapPointState::NO)) // is a wall
					continue;

				else
				{
					// get a new neighbor node
					neighbor = std::make_shared<Node>(NewPos, getHeuristicForPos(NewPos));
				}
				// if the neighbor is in the open list
				if (OPENMap_.find(neighbor->pos_.cvt2Key()) != OPENMap_.end())
				{
					if (neighbor->CurrentCost_ > Expanding->CurrentCost_ + 1)
					{
						// current cost should change
						neighbor->CurrentCost_ = Expanding->CurrentCost_ + 1;

						// update neighbor parent
						neighbor->parent_ = Expanding;
					}
				}

				else if (CLOSE_.find(neighbor->pos_.cvt2Key()) == CLOSE_.end())
				{
					neighbor->CurrentCost_ = Expanding->CurrentCost_ + 1; // cost between neigbors in this project is 1;
					neighbor->parent_ = Expanding;

					// std::cout << "Now updating node" << neighbor->x_ << " " << neighbor->y_ << std::endl;
					OPEN_.emplace(neighbor);
					OPENMap_[neighbor->pos_.cvt2Key()] = neighbor;
				}	
			}
		}

		// if endkey is found, change to passing state, push 
		if (CLOSE_.find(GoalPos_.cvt2Key()) != CLOSE_.end())
		{
			state_ = CarState::PASSING;
			std::shared_ptr<Node> current = CLOSE_.at(GoalPos_.cvt2Key());
			while (current != nullptr)
			{
				// std::cout << current->pos_.x_ << " " << current->pos_.y_ << std::endl;
				CurrentAStarPathStack_.emplace(current->pos_);
				CurrentAStarPathVector2Show_.emplace_back(current->pos_);
				current = current->parent_;
			}
			
			if (AOrAA_ == AlgorithmChoice::AA)
			{
				updateHeuristic();
			}
			return 1;
		}

		// endkey is not found, then A_star failed
		else
		{
			std::cout << "A_star can not find a path to the goal" << std::endl;
			state_ = CarState::FAIL;
			return 0;
		}
	}

	void A_star::updateHeuristic()
	{
		// for all the nodes in CLOSE
		for (const auto& n : CLOSE_)
		{
			std::string GoalKey = GoalPos_.cvt2Key();
			// if nodes is in map, update heuristic
			if (map_.find(n.first) != map_.end())
			{
				map_.at(n.first)->heuristic_ = CLOSE_.at(GoalKey)->CurrentCost_ - CLOSE_.at(n.first)->CurrentCost_;
			}

			// node is not in the map, add a new map point to the map
			else
            {   
				map_[n.first] = std::make_shared<MapPoint>(n.second->pos_,
					CLOSE_.at(GoalKey)->CurrentCost_ - CLOSE_.at(n.first)->CurrentCost_,
					MapPoint::MapPointState::NOTOBSERVED);
            }
                
        }
		
		std::cout<<'\n'<<std::endl;

	}

	
	int A_star::getHeuristicForPos(const position& pos)
	{
		// A and AA start use Manhattan.
		if (AOrAA_ == AlgorithmChoice::A || state_ == CarState::START)
			return pos.calculateManhattanDistanceTo(GoalPos_);
		// AA will use existing map heuristic
		else if (map_.find(pos.cvt2Key()) != map_.end())
			return map_.at(pos.cvt2Key())->heuristic_;

		// AA will use manhanttan when map_ has no record
		else if (map_.find(pos.cvt2Key()) == map_.end())
			return pos.calculateManhattanDistanceTo(GoalPos_);
	}


	void A_star::observeThenMove(const std::vector<std::vector<char>>& map)
	{
		CurrentAStarPathStack_.pop(); // the first is the car's current position, not useful

		std::vector<position> ObservationDirection = { position(-1, 0), position(0,-1),
			position(1, 0), position(0, 1) }; // up, left, down, right, any order, can not see diagonal 
		// observe the current postion 
		for (auto pos : ObservationDirection)
		{
			/** Two situations the MapPoint will be generated:
			1. the MapPoint is not generated and is being oberved as a wall.
			2. the MapPoint is not generated abd is being updated by aa* for heuristic

			MapPoint will be updated when:
			a. the Mappoint is already generated by observation, but aa* is updating the heuristic
			b. the Mappoint is already generated by aa* for heuristic, but now is being observed
			*/
			position NewObervationPos = CurrentCarPos_ + pos;
			if ((NewObervationPos.x_ > MapRows_ - 1) || (NewObervationPos.x_ < 0) ||  // rows out of range
				(NewObervationPos.y_ > MapCols_ - 1) || (NewObervationPos.y_ < 0))    // cols out of range
				continue;

			else if (map_.find(NewObervationPos.cvt2Key()) == map_.end() && map[NewObervationPos.x_][NewObervationPos.y_] == 'x') // 1. the MapPoint is not generated and is being oberved as a wall.
				map_[NewObervationPos.cvt2Key()] = std::make_shared<MapPoint>(NewObervationPos, getHeuristicForPos(NewObervationPos), MapPoint::MapPointState::NO); // generate

			else
			{

				if (map_.find(NewObervationPos.cvt2Key()) != map_.end())  // the mappoint is generated
				{
					if (map_.at(NewObervationPos.cvt2Key())->CanStepOn_ == MapPoint::MapPointState::NOTOBSERVED) // b. the Mappoint is already generated by aa* for heuristic, but now is being observed
					{
						if(map[NewObervationPos.x_][NewObervationPos.y_] == 'x')
							map_.at(NewObervationPos.cvt2Key())->CanStepOn_ = MapPoint::MapPointState::NO; // update observation
						else 
							map_.at(NewObervationPos.cvt2Key())->CanStepOn_ = MapPoint::MapPointState::YES;
					}
					else
						continue; // generated by observation, do nothing
				}
			}
		}

		position NextCarPosByAStar = CurrentAStarPathStack_.top();

		// check if next step is a wall or not
		if (map_.find(NextCarPosByAStar.cvt2Key()) != map_.end())
		{
			if (map_.at(NextCarPosByAStar.cvt2Key())->CanStepOn_ == MapPoint::MapPointState::NO) // is a wall
			{
				state_ = CarState::STUCK;
				// before restart a* clear all the data structure.
				clear();
			}


			// move the car to the next step and upate the MovePath vector
			else
			{
				CurrentCarPos_ = NextCarPosByAStar;
				MovePath_.emplace_back(CurrentCarPos_.x_, CurrentCarPos_.y_);
			}
		}

		else // if the mappoint is not found in the mappoint
		{
			CurrentCarPos_ = NextCarPosByAStar;
			MovePath_.emplace_back(CurrentCarPos_.x_, CurrentCarPos_.y_);
		}

		// If next step is end point 
		if (CurrentCarPos_ == GoalPos_)
			state_ = CarState::SUCESS;
	} // move


	void A_star::printMovePath(const std::vector<std::vector<char>>& map)
	{
		std::vector<std::vector<char>> TempMap = map;
		std::cout << "The numebr of expanded cells: " <<
			Answer2ProjectQuestions.NExpandedCells << std::endl;
		std::cout << "The numebr of searches: " <<
			Answer2ProjectQuestions.NSearches << '\n' << std::endl;

		for (auto& pos : MovePath_)
		{
			if (map[pos.x_][pos.y_] != 's' && map[pos.x_][pos.y_] != 'g')
				TempMap[pos.x_][pos.y_] = 'O';
		}
		std::cout << "The actual moving path:" << std::endl;
		Util::printMap(TempMap);
	}

	void A_star::printCurrentProposedPath() const
	{
		// construct a initial map with all points unblocked
		std::vector<std::vector<char>> map(MapRows_, std::vector<char>(MapCols_, '_'));
		// update the map observed part
		for (const auto& n : map_) 
		{
			if (n.second->CanStepOn_ == MapPoint::MapPointState::NO)
				map[n.second->pos_.x_][n.second->pos_.y_] = 'x';
		}

		// draw the proposed path on the map
		for (const auto& n : CurrentAStarPathVector2Show_)
		{
			map[n.x_][n.y_] = 'O';
		}

		map[CurrentCarPos_.x_][CurrentCarPos_.y_] = 's';
		map[GoalPos_.x_][GoalPos_.y_] = 'g';

		std::cout << "Newly generated path, after start or block:" << std::endl;
		Util::printMap(map);
	}

    void A_star::printCurrentMapHeurisic() const
    {
        std::vector<std::vector<int>> map(MapRows_, std::vector<int>(MapCols_, -1));
        for (const auto& n : map_) 
		{
				map[n.second->pos_.x_][n.second->pos_.y_] = n.second->heuristic_;
		}
        
        for(int i = 0; i < map.size(); i++)
        {
            for (int j = 0; j< map[0].size(); j++)
            {
                std::cout << map[i][j]<<" ";
            }
            std::cout<<'\n'<<std::endl;
        }
    }
    
    
	int A_star::run(const std::vector<std::vector<char>>& map)
	{
		if (AOrAA_ == AlgorithmChoice::A)
			std::cout << "The A* alogrithm result:" << '\n' << std::endl;
		else
			std::cout << "The AA* alogrithm result:" << '\n' << std::endl;

		while (!(state_ == CarState::SUCESS || state_ == CarState::FAIL))
		{


			switch (state_)
			{
				case CarState::START:
				case CarState::STUCK:
                    findPath();
                    Answer2ProjectQuestions.NExpandedCells += CLOSE_.size();
					Answer2ProjectQuestions.NSearches++;
					printCurrentProposedPath();
                    // printCurrentMapHeurisic();
					break;
				case CarState::PASSING: 
					observeThenMove(map); 
					break;
			}
		}

		if (state_ == CarState::SUCESS)
		{
			printMovePath(map);
			return 1;
		}
		else 
			return 0;
	} // run



} //namespace AStarAlgorithms
