/**
This file is the implementation of A* algorithm.
Several points are very important:
1. Since the observation map matrix, the heuristic matrix, and open lose list are all sparse matrix,
   I stored all the matrix by using nodes to save the memory. So there is no 2d vector in this 
   algorithm.
2. OPEN and CLOSE list are working together. So I define the nodes for them as Node Class. The node
   will not survive after one run of A*.
   So another class mappoint is used to store the map information we get and the heuristic we have
   updated. Mappoints are stored in map list for next run of A*
3. All the list except OPEN are constructed by hash map -- unordered_map. The key is the position
   of the nodes, I have changed the postion into string like "2A", "4D". OPEN is stored in Priority 
   queue for picking and also a map for fast indexing, since the queue and map values are just pointers,
   it will not cost too much to the memory while I can search one value much faster.
4. a* and aa* the main difference is that after one run. aa* will update the heuristic of mappoint.
*/

#ifndef A_STAR_H
#define A_STAR_H

#include <unordered_map>
#include <vector>
#include <queue>
#include <functional>
#include <memory>
#include <stack>
#include <iostream>
#include "Points.h"
#include "util.h"

namespace AStarAlgorithms
{

    
	class A_star
	{
        private:
        
        enum AlgorithmChoice                                  // enum class to choose the algorithm
        {
            A,
            AA
        }AOrAA_;

		enum CarState 
		{ START, PASSING, STUCK, SUCESS, FAIL } state_;				// A_star algorithm running state
		
		position CurrentCarPos_;									// the car's current pos, initialized with start point
		position GoalPos_;												// the goal

		int getHeuristicForPos(const position& pos);							// for aa* and a*, this function is different 

		std::unordered_map<std::string, std::shared_ptr<MapPoint>> map_;							// this map is for observation, assume we don't have
																				// have any knowledge about the map at first
																				// key: 1A 5B...
																				// value: shared_ptr to nodes
		int MapRows_;															// the rows of the map, since we dont have matrix, this info is needed
		int MapCols_;															// the cols of the map, since we dont have matrix, this info is needed


		std::unordered_map<std::string, std::shared_ptr<Node>> CLOSE_;			// CLOSE list, use set for indexing
																			// key: 1A 5B...
																			// value: shared_ptr to nodes
		/*
		class NodeComparer											// the original functor for priority queue to compare
		{
		public:
			bool operator() (const std::shared_ptr<Node>& n1, const std::shared_ptr<Node>& n2)
			{
				return (n1->heuristic_+ n1->CurrentCost_ > n2->heuristic_ + n2->CurrentCost_);
			}
		};
		*/
		
        // this is for question3, add it later.
		class NodeComparer											// the functor for priority queue to compare 
		{															// favor greater heuristic
		public:
			bool operator() (const std::shared_ptr<Node>& n1, const std::shared_ptr<Node>& n2)
			{
				int AddedCostN1 = n1->heuristic_ + n1->CurrentCost_;
				int AddedCostN2 = n2->heuristic_ + n2->CurrentCost_;
				if (AddedCostN1 != AddedCostN2)
					return (AddedCostN1 > AddedCostN2);       // smaller total cost
				else
					return (n1->heuristic_ < n2->heuristic_); // greater heuristic
			
                
            }    
		};
		

		std::priority_queue<std::shared_ptr<Node>, std::vector<std::shared_ptr<Node>>, NodeComparer> OPEN_;
		std::unordered_map<std::string, std::shared_ptr<Node>> OPENMap_;	// Duplicate set for open to make indexing faster

		/** Description: Destructor
		All the data will be cleared automatically
		*/
		// virtual ~A_star() {}


		/** Description: clear all the date structures a* has, so that the algorithm can rerun
		*/
		void clear();
		

		/** Description: the most important function for A_star to get the path from current observation map
			@return: int, 1 for success, 0 for no path to the end point
		*/
		int findPath();

		std::stack<position> CurrentAStarPathStack_;	// the path given by a* algorithm
		std::vector<position> CurrentAStarPathVector2Show_; // the vector path to print to users
															// only for print functions

		std::vector<position> MovePath_;	// the path the car is really moving on
		

		/** Description: Update the heuristic based on Goal's current cost. Heuristic = Last node cost - current cost;
		*/
		void updateHeuristic();
											
											
		/** Description: Obervation and move function, the mission:
			@params: map, the map to observe
			1. observe the map from current location
			2. decide weather the car gets stuck and need to rerun findPath() function.
		*/
		void observeThenMove(const std::vector<std::vector<char>>& map);

		/** Description: Print the current path proposed by A_star
			@params: map, 2d vector of the given map
		*/
		void printCurrentProposedPath() const;


		/** Description: Print the final path got by A_star
			@params: map, 2d vector of the given map
		*/
		void printMovePath(const std::vector<std::vector<char>>& map);
        
        
        /** Description: Print the final path got by A_star
			@params: map, 2d vector of the given map
		*/
        void printCurrentMapHeurisic() const;
		

	public:
		/** Description: constructor
		@params MapRows: the number of given map rows
		@params MapCols: the number of given map cols
		@params start: the postion of the start point
		@params goal: the postion of the goal point
		@params AlgorithmChoice: 1 for AA, 0 for A
		At the beginning we should start only with rows and cols,
		The observation map_ will be all 0s
		*/
		A_star(int MapRows, int MapCols, position start, position goal, int AlgorithmChoice) : state_(CarState::START),
			MapRows_(MapRows), MapCols_(MapCols), CurrentCarPos_(start), GoalPos_(goal)
		{
			MovePath_.emplace_back(start.x_, start.y_);
			if (AlgorithmChoice == 1)
				AOrAA_ = AA;
			else AOrAA_ = A;
		}


		/** Description: Run A_star algorithm and get the result.
		*/
		int run(const std::vector<std::vector<char>>& map);

		ProjectQuestions Answer2ProjectQuestions;

	};

}

#endif // !A_STAR_H
