#include "util.h"

namespace AStarAlgorithms
{
	namespace Util
	{
		void printMap(const std::vector<std::vector<char>>& map)
		{
			for (int i = 0; i < map.size(); i++)
			{
				for (int j = 0; j < map[0].size(); j++)
				{
					std::cout << map[i][j] << " ";
				}
				std::cout << '\n';
			}

			for (int i= 0 ; i< 5; i++)
			{
			std::cout << std::endl;
			}

		} // end function printMap

		int readMapFile(const std::string MapFile, std::vector<std::vector<char>> & map)
		{
			std::ifstream InputFileStream(MapFile);
			if (!InputFileStream.is_open())
			{
				std::cout << "Couldn't open " << MapFile << std::endl;
                return 0;
			}

			int  RowNumber, ColNumber;
			InputFileStream >> RowNumber;
			InputFileStream >> ColNumber;

			for (int i = 0; i < RowNumber; i++)
			{
				map.emplace_back(0);
				for (int j = 0; j < ColNumber; j++)
				{
					char TempChar;
					InputFileStream >> TempChar;
					map[i].push_back(TempChar);
				}
			}
			
			return 1;
		} // end function readMapFile

		int findMapStartAndEnd(const std::vector<std::vector<char>> & map, position& start, position& goal)
		{
			for (int i = 0; i < map.size(); i++)
			{
				for (int j = 0; j < map[0].size(); j++)
				{ 
					if (map[i][j] == 's')
					{
						start.x_ = i;
						start.y_ = j;
					}

					else if (map[i][j] == 'g')
					{
						goal.x_ = i;
						goal.y_ = j;
					}
				}
			}
			return 1;
		} // end function findMapStartAndEnd




	}// end namspace Util

}
