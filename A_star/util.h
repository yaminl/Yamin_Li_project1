/** This file is to define some util functions which is used to:
	1. read the map file.
	2. print the map.
	3. find the start and end point of the map
*/

#pragma once
#ifndef UTIL_H
#define UTIL_H
#include <vector>
#include <iostream>
#include <fstream>
#include "Points.h"

namespace AStarAlgorithms
{
	namespace Util
	{

		/** Description: A simple function for printing the map to the screen
			@params: map, the input 2d vector
		*/
		void printMap(const std::vector<std::vector<char>>& map);

		/** Description: read the input map file
			@params: MapFile, the directory of the map file
			@params: map, the input 2d vector map
		*/
		int readMapFile(const std::string MapFile, std::vector<std::vector<char>> & map);

		/** Description: find the start point and end point of the map
			@params: map, the 2d vector representing the map
		*/
		int findMapStartAndEnd(const std::vector<std::vector<char>> & map, position& start, position& goal);
	}
}

#endif // !UTIL_H
