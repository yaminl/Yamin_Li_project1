THis is the Project 1 of Laioffer Self driving car course.


To compile the file:
in the cmd line:
1. go to the main directory of the project:
    cd project_directory
2. create a new empty release directory:
    mkdir release
    cd release
3. call cmake:
    Cmake ..
4. make file:
    make
    
Then the binary should be in the release directory.
You can run the executable by enter in the cmd line:
A_star_executable -i ../inputs/8.txt