// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
#include "A_star/util.h"
#include "A_star/A_star.h"
#include "A_star/Points.h"
#include <iostream>


int main(int argc, char* argv[])
{
    // check if the argument number is 3 or 4 if not number is wrong.
    if (argc != 4 && argc != 3)
    {
        std::cout << "The number of argument is not correct, exit!" << std::endl;
        return 1;
    }
    
    // check if the 3rd argument is -A
    std::string TempString;
    int isAA = 0;
    if (argc == 4)
    {
        TempString = argv[3];
        if (TempString != "-A")
        {
            std::cout << "The second argument has to be -A, exit!" << std::endl;
            return 1;
        }
        else
            isAA = 1;
    }
    
    // check if the 1st argument is -i
    TempString = argv[1];
    if (TempString != "-i")
    {
        std::cout << "The second argument has to be -i, exit!" << std::endl;
        return 1;
    }
    
    // pass the file name to the algorithm and see if the file is good
    TempString = argv[2];
    
    std::cout << "Reading the input file..." << std::endl;
    // initialize a 2d vector to store the map
    std::vector<std::vector<char>> map(0);
    // read the map 
    int result = AStarAlgorithms::Util::readMapFile(TempString, map);
    if (result == 1)
    {
        std::cout << "File loaded successfully!" << std::endl;
        std::cout << "\n\n\n" << std::endl;
    }
    else 
    {
        std::cout << "File loading failed, exit!" << std::endl;
        return 1;
    }
    
    
    //---------------now below is the algorithm run -------------
    
    std::cout << "Orignal Map:" << std::endl;
    AStarAlgorithms::Util::printMap(map);
    
    // get the start and end point 
    AStarAlgorithms::position start, goal;
    AStarAlgorithms::Util::findMapStartAndEnd(map, start, goal);

    // initialize A*
    AStarAlgorithms::A_star A(map.size(), map[0].size(), start, goal, isAA);
    A.run(map);
    
    // have to enter something to end the program
	int i; std::cin >> i;
	return 1;
}

